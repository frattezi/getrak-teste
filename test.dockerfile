FROM python:3.7-alpine

COPY . /project
WORKDIR /project

RUN pip install . && pip install pytest

ENTRYPOINT [ "pytest", "/project/tests"]


