#!/usr/bin/env python

import argparse
from starshipfacilities.starshipfacilities import StopCalculator

def main_parser():
    calculator = StopCalculator()
    description = ('A simple command line interface for starships enthusiasts')
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        'distances',
        metavar='N',
        type=int,
        nargs='+',
        help='Distances list to calculate in MGLT'
    )

    args = parser.parse_args()
    for distance in args.distances:
        starshiplist = calculator.calculate_stops(distance)
        for starship in starshiplist:
            print("{}: {}".format(starship['Name'], starship['Stops']))
        print('-----------###----------')


if __name__ == "__main__":
    main_parser()