# STARSHIP-FACILITIES

Python facility for starship travelers!

Need to know how many stops to Tatooine or maybe Alderaan? Now you can
do it fast and easy.

This libary can be used as a CLI or imported into your python code.

## CLI tool

For CLI usage you need to first install the requirements for python, to do so run:

```bash
pip install .
```

First try your installation running the help command.

```bash
python cli.py --help
```

Expected output:

```bash
usage: cli.py [-h] N [N ...]

A simple command line interface for starships enthusiasts

positional arguments:
  N           Distances list to calculate in MGLT

optional arguments:
  -h, --help  show this help message and exit
```

Now you can use ou calculation tool, just type the number or numbers to be
calculated:

```bash
python cli.py 1000000 10
```

The command will return a list of ships containing the result of the calculations.
Unfortunetly we don't have all the data we wanted to, so some results will display NA.


---

## Using as a python packge

You can use this packge inside your code too. Append it to your systems so you can
easily calculate the number of stops needed to the next outpost.

First install python packge:

```bash
git clone https://gitlab.com/frattezi/getrak-teste.git
cd getrak-teste
pip install .
```

This will install the libary into your env (not using virtualenvs?
Well you should, visit this: [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/))

Now you can use it in your python scrips by importing it.
Here is a simple example using python 3:

Let's say you already have some inside data like, how many food you buy on average
per stop you do and you want to have some predictability on your budget.

You can use the starshipfacilities to help you with that!

Considering `self.food_bought_per_stop` the data I was talking about earlier
you could simply create a new method in your system to predict your future spendings.

```python
from starshipfacilities.starshipfacilities import StopCalculator

def PreviewFoodBudget(distance):
  stop_calculator =  StopCalculator()
  stops_needed_per_ship = stop_calculator.calculate_stops(distance)

  # This returns the ship model equal to mine
  my_ship = [ship for ship in stops_needed_per_ship if my_ship in ship['Name']]

  total_food_for_travel = my_ship[0]['Stops']*self.food_bought_per_stop

  return total_food_for_travel
```

This is just the beggining, use your imagination!

---

## Docker

We have a docker distribution to!

You can use it by:

```bash
# Presuming you cloned the project and youre on /getrak-teste
docker build -t <container-name>:<container:tag> .
docker run <container-name>:<container:tag> N1 N2 ...
```
*N1, N2 ... NN are distances to be used as parameters (in MGLT)*

Docker is the easiest way to get our product, No more excuses.

### Docker for tests!
You can run tests using pytests but meh

We have a container just for it:

```bash
docker buld -f test.dockerfile -t startests:latest
docker run startests:latest
```

You will be prompt with the test results. It updates the tests dir
on every build

---

## Gitlab CI

We have a basic ci, tests on every branch and for merging into master.
The deploy CI is just a test mock, for pipeline demonstrantion.

### May the force be with you