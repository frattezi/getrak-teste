FROM python:3.7-alpine

COPY . /project
WORKDIR /project

RUN pip install .

CMD [ "1000000" ]

ENTRYPOINT [ "python", "/project/cli.py"]
