#!/usr/bin/env python
import unittest
from unittest.mock import patch, Mock

from  starshipfacilities.starshipfacilities import StopCalculator

class TestStarshipFacilities(unittest.TestCase):

    mock_request_return = [
        {'Name': 'Executor', 'Status': 'OK', 'MGLT': '40', 'Resources': '6 years'},
        {'Name': 'Death Star', 'Status': 'Invalid', 'MGLT': '10', 'Resources': '3 years'},
        {'Name': 'Sentinel-class landing craft', 'Status': 'Invalid', 'MGLT': '70', 'Resources': '1 month'},
        {'Name': 'Millennium Falcon', 'Status': 'Ok', 'MGLT': '75', 'Resources': '2 months'}
    ]

    expected_result = [
        {'Name': 'Executor', 'Stops': 0},
        {'Name': 'Death Star', 'Stops': 'NA'},
        {'Name': 'Sentinel-class landing craft', 'Stops': 'NA'},
        {'Name': 'Millennium Falcon', 'Stops':9}
    ]

    @patch('starshipfacilities.starshipfacilities.SwapiRequestHelper')
    def test_calculate_stops(self, mock_request):
        """Test the main function."""
        test_class = StopCalculator()

        # Mocks request return
        mocked_requester = Mock()
        mocked_requester.populate_starship_list.return_value = self.mock_request_return
        mock_request.return_value = mocked_requester

        result = test_class.calculate_stops(1000000)

        assert result == self.expected_result


if __name__ == '__main__':
    unittest.main()