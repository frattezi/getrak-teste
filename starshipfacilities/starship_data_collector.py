import requests


class SwapiRequestHelper(object):
    """
    Request helper for starshipfacilities.

    This class manages starshipfacilities connections and requests with swapi
    """

    def __init__(self):
        """Init SwapiRequestHelper"""
        self.base_url = 'https://swapi.co/api/starships/'

    @staticmethod
    def _data_is_valid(data):
        """Check if the starship have minimum data requirements."""
        if data["MGLT"] == "unknown" or data["consumables"] == "unknown":
            return False
        return True

    def populate_starship_list(self):
        """
        Returns a list containing all starships availble with basic data.

        Having basic data means that the ship contais the MGLT
        and consumables fields info.
        """
        starship_list = []
        base_url = self.base_url

        while True:
            request = requests.get(base_url).json()
            data = request['results']

            if not data:
                raise NotImplementedError

            for starship in data:
                starship_list.append(self.format_starship_data(starship))

            base_url = request['next']

            if not base_url:
                break
        return starship_list

    def format_starship_data(self, starship):
        """
        Format starship data.

        This function returns a dictionary containing a
        """
        if self._data_is_valid(starship):
            status = "OK"
        else:
            status = "Invalid"

        starship_valuable_data = {
            "Name": starship['name'],
            "Status": status,
            "MGLT": starship['MGLT'],
            "Resources": starship['consumables'],
        }
        return starship_valuable_data
