from .starship_data_collector import SwapiRequestHelper

class StopCalculator(object):

    @staticmethod
    def _turn_consumables_to_hours(consumables):
        """Simple conversion of consubles to hours"""
        days_in_year = lambda years: years*365*24
        days_in_month = lambda months: months*30*24
        days_in_week = lambda weeks: weeks*7*24

        if  any(string in consumables for string in ["years", "year"]):
            return days_in_year(int(consumables.split(' ')[0]))

        if any(string in consumables for string in ["months", "month"]):
            return days_in_month(int(consumables.split(' ')[0]))

        if any(string in consumables for string in ["weeks", "week"]):
            return days_in_week(int(consumables.split(' ')[0]))

        if any(string in consumables for string in ["days", "day"]):
            return int(consumables.split(' ')[0])

    def calculate_stops(self, distance):
        """
        Calculate the number of stops for each ship for a given distance.

        Returns a list containing a dicts formated like:
        {
            'Name': Ship Name
            'Stops': Number of stops calculated
        }
        """
        requester = SwapiRequestHelper()
        starship_list = requester.populate_starship_list()
        result_list = []

        for ship in starship_list:
            if ship["Status"] == "Invalid":
                result_list.append({'Name': ship['Name'], 'Stops': 'NA'})
            else:
                number_of_stops = (
                    (distance//int(ship['MGLT'])//
                     self._turn_consumables_to_hours(ship['Resources'])
                    )
                )
                result_list.append({'Name': ship['Name'], 'Stops': number_of_stops})
        return result_list
