"""
CREATED AT: 2019/04

Configuration for python packaging,
Defines a simple package for the application. 
"""
from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='starshipfacilities',
    version='0.0.1',
    install_requires=requirements,
    description='Starship facilities for starship enthusiats',
    license='MIT',
    packages=['starshipfacilities'],
    author='Pedro Frattezi',
    author_email='pedro.frattezi@gmail.com',
    keywords=['star wars'],
    url='https://gitlab.com/frattezi/getrak-teste'
)